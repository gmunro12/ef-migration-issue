﻿using System.ComponentModel.DataAnnotations;

namespace v2
{
    public abstract class AbstractIdentifiable<T>
    {
        [Key]
        public virtual T Id { get; set; }
    }
}
