﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace v2.Migrations
{
    public partial class SpuriousMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProvisionalUsers_Organisations_OrganisationId",
                table: "ProvisionalUsers");

            migrationBuilder.AlterColumn<int>(
                name: "OrganisationId",
                table: "ProvisionalUsers",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_ProvisionalUsers_Organisations_OrganisationId",
                table: "ProvisionalUsers",
                column: "OrganisationId",
                principalTable: "Organisations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProvisionalUsers_Organisations_OrganisationId",
                table: "ProvisionalUsers");

            migrationBuilder.AlterColumn<int>(
                name: "OrganisationId",
                table: "ProvisionalUsers",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ProvisionalUsers_Organisations_OrganisationId",
                table: "ProvisionalUsers",
                column: "OrganisationId",
                principalTable: "Organisations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
