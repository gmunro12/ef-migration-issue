﻿using System.ComponentModel.DataAnnotations;

namespace v2.Models
{
    public class Organisation: AbstractIdentifiable<int>
    {
        [Key]
        public override int Id { get; set; }
    }
}
