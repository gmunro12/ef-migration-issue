﻿using System.ComponentModel.DataAnnotations;

namespace v2.Models
{
    public class ProvisionalUser : AbstractIdentifiable<int>
    {
        [Key]
        public override int Id { get; set; }
        [Required]
        public string Forename { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public virtual Organisation Organisation { get; set; }

    }
}
