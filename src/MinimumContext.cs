﻿using Microsoft.EntityFrameworkCore;
using v2.Models;

namespace v2
{

        public class MinimumContext : DbContext
        {
            public MinimumContext(DbContextOptions<MinimumContext> options) : base(options)
            {
            }

            public DbSet<Organisation> Organisations { get; set; }
            public DbSet<ProvisionalUser> ProvisionalUsers { get; set; }
        }
}